'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
require('dotenv').config()
const token =`${process.env.access_token}`

const request = require('request');
const fs = require('fs')

const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));

server.use(bodyParser.json());
const apiUrl = `${process.env.apiUrl}`

// let nama = ''
let comment = ''
function sendMsg(msg,room_id){
    request.post(
        apiUrl+'post_comment',
        { 
            json:true,
            body:{
                access_token:token,
                topic_id:room_id,
                type:'text',
                comment:msg
            }
        },
    );
}

//Contoh pesan dengan button
function sendMsgBtn(msg, room_id, payload) {
    request.post(
        apiUrl + 'post_comment', {
            json: true,
            body: {
                access_token: token,
                topic_id: room_id,
                type: 'buttons',
                comment: msg,
                payload: JSON.stringify(payload)
            }
        },
    );
}
function sendMsgCarousel(msg,room_id,payload){
    request.post(
        apiUrl + 'post_comment', {
            json: true,
            body: {
                access_token: token,
                topic_id: room_id,
                type: 'carousel',
                comment: msg,
                payload: JSON.stringify(payload)
            }
        },
    );
}
server.post('/',(req,res)=>{
    // Bot terima input dari user berupa room_id, pesan, dan nama user
    let room_id = res.req.body.chat_room.qiscus_room_id
    let msg = res.req.body.message.text
    let nama = res.req.body.from.fullname

    //konversi pesan yang mengandung huruf kapital menjadi huruf kecil
    let lower = msg.toLowerCase()

    //matching pesan yang mengandung kata-kata berikut menggunakan regex
    let matchHi = lower.match(/halo|hi|hai|oi/g)
    let menu = lower.match(/menu|ada apa|bisa apa|bisa/g)
    let thx = lower.match(/terima kasih|thanks|thank|thx|terima kasih banyak/g)

    // check apakah pesan mengandung kata-kata diatas, kemudian mengirim pesan
    if(matchHi !== null){
        comment = 'halo ' + nama + ' ! Aku asisten pribadi mu, ada yang bisa dibantu?'
        payload = {
            'text': comment,
            'buttons': [{
                    'label': 'Tombol Reply Text',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Tombol Link',
                    'type': 'link',
                    'payload': {
                        'url': 'https://www.google.com',
                    }
                }
            ]
        }
        sendMsgBtn(comment,room_id,payload)
    }
    else if(menu !==null){
        comment = "Aku Bisa melakukan ini"
        payload = {
            'cards' : [
                {
                    'image' : 'https://cdns.img.com/a.jpg',
                    'title' : 'Gambar 1',
                    'description' : 'Carousel Double Button',
                    'default_action' : {
                        'type' : 'postback',
                        'postback_text' : 'Load More...',
                        'payload' : {
                            'url' : 'https://j.id',
                            'method' : 'GET',
                            'payload': null
                        }
                    },
                    'buttons' : [
                        {
                            'label' : 'Button 1',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        },
                        {
                            'label' : 'Button 2',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        }
                    ]
                },
                {
                    'image' : 'https://res.cloudinary.com/hgk8.jpg',
                    'title' : 'Gambar 2',
                    'description' : 'Carousel single button',
                    'default_action' : {
                        'type' : 'postback',
                        'postback_text' : 'Load More...',
                        'payload' : {
                            'url' : 'https://j.id',
                            'method' : 'GET',
                            'payload': null
                        }
                    },
                    'buttons' : [
                        {
                            'label' : 'Button 1',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        }
                    ]
                }
            ]
        }
        sendMsg(comment,room_id)
        sendMsgCarousel(comment,room_id,payload) 
    }
    else if(thx !== null){
        comment = 'Terima kasih '+nama+' !'
        sendMsg(comment,room_id)
    }
    else{
        comment = 'Mohon Maaf! '+nama+', Perintah tidak dimengerti, coba say hi!'
        sendMsg(comment)
    }
})

server.listen((process.env.PORT || 3000), () => {
    console.log("Server is up and running..."+`${process.env.PORT}`);
});
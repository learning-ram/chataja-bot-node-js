# ChatAja Bot with Node JS

### Requirements
 - [NodeJS](https://nodejs.org/dist/v10.16.3/node-v10.16.3-x64.msi)
 - [ngrok](https://ngrok.com/)
 - Bot Access Token (you can chat with Chatbot Builder in `Jelajah`menu)
### How to Run
 - After install all requirements, you can clone this repository
 ```bash
$ git clone https://gitlab.com/learning-ram/chataja-bot-node-js.git
$ cd chataja-bot-node-js
```
* run `npm install` on your project 
* Go to `Jelajah` menu on your phone
* chat with `Chatbot Builder`
* Create bot and get `access_token`
* Copy and paste to `.env` on your root folder
* Run server
```bash
$ npm start
```
- Tunneling your webhook server
```bash
$ ngrok http 3000
 ```
- Register your webhook url by copy your ngrok https url from CLI, then input it to `Chatbot Builder`
- Let's Coding on `index.js`